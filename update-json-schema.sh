#!/bin/bash
# Fetch kubernetes json schema from a remote git repository
set -e

SOURCE_GIT_URL=https://github.com/yannh/kubernetes-json-schema.git

# For sparse-checkout, at least git 2.25.0 is required
GIT_MIN_VERSION="2.25.0"
if ! command -v git &> /dev/null || !(echo aa_min version ${GIT_MIN_VERSION}; git --version) | sort -Vk3 | tail -1 | grep -q git; then
    echo "git >= ${GIT_MIN_VERSION} is required"
    exit 1
fi

if [ "$1" == "-h" -o "$#" -lt 1 ]; then
    this="$(basename $0)"
    echo "${this} will download JSON schema from ${SOURCE_GIT_URL} to ${SCHEMA_CACHE}"
    echo "Usage: ${this} <kubernetes-version>[ <kubernetes-version> ...]"
    echo "<kubernetes-version> must be on form of: vX.Y.Z"
    exit 0
fi

# We need to fetch the base and the -standalone-strict schema for each kubernetes version
schema_versions="$@ ${@/%/-standalone-strict}"

tmpdir=$(mktemp -d)
trap cleanup SIGINT SIGTERM ERR EXIT

cleanup() {
    trap - SIGINT SIGTERM ERR EXIT
    rm -rf "$tmpdir"
}

git clone --quiet --filter=blob:none --no-checkout "$SOURCE_GIT_URL" "$tmpdir"
pushd "$tmpdir" > /dev/null
git sparse-checkout set --cone
git checkout --quiet master

echo "Fetching ${schema_versions}"
git sparse-checkout set $schema_versions
popd > /dev/null

for version in $schema_versions; do
    if ! [ -d "${tmpdir}/${version}" ]; then
        echo "ERROR: Version ${version} not found"
        continue
    fi
    rm -rf "$version"
    cp -a "${tmpdir}/${version}" .
done