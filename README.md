# Kubernetes JSON Schemas

This is a partial copy of Kubernetes JSON Schemas as found on:
* https://github.com/instrumenta/kubernetes-json-schema
* https://github.com/yannh/kubernetes-json-schema

The schemas are used by [kubeconform](https://gerrit.wikimedia.org/g/operations/debs/kubeconform) to validate Kubernetes manifests.

To add a new Kubernetes version, run:
```
./update-json-schema <kubernetes-version>[ <kubernetes-version> ...]
```

and then commit each version to the git repo and `git push`.